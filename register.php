<?php

require_once "MamarazziDatabase.php";

function register($database, $username, $password, $email) {
	$response = array("request" => "register");

	if ($database->usernameExists($username) == true) {
		$response['success'] = false;
		$response['error'] = 1;
		$response['error_message'] = "A user with that username already exists.";
		return $response;
	}
	else if ($database->emailExists($email) == true) {
		$response['success'] = false;
		$response['error'] = 2;
		$response['error_message'] = "A user with that e-mail address already exists.";
		return $response;
	}
	
	$database->registerUser($username, $password, $email);

	$response['success'] = true;
	$response['error'] = 0;
}

?>