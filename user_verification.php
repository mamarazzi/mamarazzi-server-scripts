<?php

require_once "MamarazziDatabase.php";


if (isset($_POST['request']) && $_POST['request'] != "") {
	try {
		$database = new MamarazziDatabase();
	}
	catch (Exception $e) {
		echo "Exception: " . $e;
	}

	if ($_POST['request'] == "login") {
		require_once "login.php";
		$response = login($database, $_POST['login'], $_POST['password']);
		echo json_encode($response);
	}
	else if ($_POST['request'] == "register") {
		require_once "register.php";
		$response = register($database, $_POST['username'], $_POST['password'], $_POST['email']);
		echo json_encode($response); 
	}
	else {
		echo "Unrecognized tag.";
	}
}
else {
	echo "No request was sent.";
}


?>