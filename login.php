<?php

require_once "MamarazziDatabase.php";

function login($database, $login, $password) {
	$response = array("request" => "login");

	$unique_id = $database->verifyUser($login, $password);

	if ($unique_id != "") {
		$userDetails = $database->getUserDetails($unique_id);

		$response['success'] = true;
		$response['error'] = 0;
		$response['user_details'] = $userDetails;
	}
	else {
		$response['success'] = false;
		$response['error'] = 1;
		$response['error_message'] = "Wrong login or password";
	}
	return $response;
}

?>