<html>
	<body>
		This is totally not password protected, and if you are not Emma, you really shouldn't be here. Shoo with you!
		<h4>Add mission</h4>
		<form action="mission_manager.php" method="post">
			Mission ID: <input name="id" type="text" />
			Title: <input name="title" type="text" />
			Level Requirement: <input name="level" type="text" />
			<input type="hidden" name="request" value="add" />
			<input type="submit" />
		</form>
		<br/>
		<h4>Remove mission</h4>
		<form action="mission_manager.php" method="post">
			Mission ID: <input name="id" type="text" /> 
			<input type="hidden" name="request" value="remove" />
			<input type="submit" />
		</form>


		<br/>

		<h4>Missions in database<h4/>
		<table style="border-style:dashed; border-color:#34A7FF; border-width:2px">
			<tr style="border-style:inherited;">
				<td><b>ID</b></td>
				<td><b>Title</b></td>
				<td><b>Level</b></td>
			<tr/>
			<?php
				require_once "MamarazziDatabase.php";
				$database = new MamarazziDatabase();
				$missions = $database->getMissions(array());

				foreach ($missions as $mission) {
					echo "<tr>";
					echo "<td>" . $mission['mission_id'] . "</td>";
					echo "<td>" . $mission['title'] . "</td>";
					echo "<td>" . $mission['level'] . "</td>";
					echo "</tr>";
				}


			?>
		</table>

	</body>
</html>