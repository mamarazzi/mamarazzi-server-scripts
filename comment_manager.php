<?php

if (!empty($_POST['request'])) {
	require "MamarazziDatabase.php";
	$database = new MamarazziDatabase();

	$response = array();
	$response['request'] = $_POST['request'];

	if ($_POST['request'] == "add") {
		$user_id = $_POST['user_id'];
		$picture_id = $_POST['picture_id'];
		$comment = $_POST['comment'];

		$database->addComment($picture_id, $user_id, $comment);

		$response['success'] = true;

		echo json_encode($response);
	}
	else if ($_POST['request'] == "get") {
		$response['success'] = true;

		$picture_id = $_POST['picture_id'];

		$result = $database->getComments($picture_id);

		$response['comments'] = $result;

		echo json_encode($response);
	}
}
else {
	echo "No request specified";
}

?>