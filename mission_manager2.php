<?php

if (isset($_POST['request']) && $_POST['request'] != '') {
	require_once "MamarazziDatabase.php";
	$manager = new MamarazziDatabase();

	$response = array();

	if ($_POST['request'] == "add") {
		$response['request'] = "add";

		$missionId = $_POST['id'];
		$title = $_POST['title'];
		$level = $_POST['level'];
		$success = $manager->addMission($missionId, $title, $level);
		if ($success == true) {
			$response['success'] = 1;
		}
		else {
			$response['success'] = 0;
		}

		echo json_encode($response);
	}
	else if ($_POST['request'] == "remove") {
		$response['request'] = "remove";

		$missionId = $_POST['id'];
		$success = $manager->removeMission($missionId);

		if ($success == true) {
			$response['success'] = 1;
		}
		else {
			$response['success'] = 0;
		}

		echo json_encode($response);
	}
	else if ($_POST['request'] == "list") {
		$response['request'] = "list";

		$excludeList = json_decode($_POST['exclude']);

		$missions = $manager->getMissions($excludeList);
		$response['success'] = 1;
		$response['missions'] = $missions;

		echo json_encode($response);
	}
	else {
		$response['request'] == "unknown";
		$response['success'] = 0;

		echo json_encode($response);
	}
}
else {
	echo "Error: No request given.";
}

?>