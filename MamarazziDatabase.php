<?php

	require_once "database_config.php";
	require_once "DatabaseHandler.php";

class MamarazziDatabase {
	
	private $database = 0;

	public function __construct() {
	
		$databaseHandler = new DatabaseHandler();
		$databaseHandler->connect(DATABASE_DSN, DATABASE_USER, DATABASE_PASS);
		try {
			$this->database = $databaseHandler->getPDO();
		}
		catch (Exception $e) {
			echo "Exception: " . $e;
		}
	}

	//////////////////////////////////////////////////////////////
	// Authentication methods used for authentication purposes. //
	//////////////////////////////////////////////////////////////


	// Verifies user by login (either email or username) and returns the users unique id on success.
	// Empty string on failure.
	public function verifyUser($login, $password) {
		if ($this->usernameExists($login)) {
			$loginColumn = "username";
		}
		else if ($this->emailExists($login)) {
			$loginColumn = "email";
		}
		else {
			return "";
		}

		try {
			$statement = $this->database->prepare("SELECT id, encrypted_password, salt FROM users WHERE $loginColumn = ?");
			$statement->execute(array($login));
			$result = $statement->fetch(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {
			echo "Exception: " . $e;
		}

		$testPassword = crypt($password, $result['salt']);

		if($testPassword == $result['encrypted_password']) {
			return $result['id'];
		} 
		else {
			return "";
		}
	}

	// Returns the user details in the form of an associative array, by the user id.
	public function getUserDetails($userId) {
		try {
			$statement = $this->database->prepare("SELECT id, username, email, experience, completed, profile_image FROM users WHERE id = ?");
			$statement->execute(array($userId));
			$results = $statement->fetch(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {
			echo "Exception: " . $e;
		}

	
		$arrayCompleted = json_decode($results['completed']);
		$results['completed'] = $arrayCompleted;

		return $results;
	}

	// Registers user in database. Checking for availability is not done here.
	public function registerUser($username, $password, $email) {
		require_once "database_config.php";
		echo "ENCRYPT_COST " . ENCRYPT_COST . "\n"; 
		try {
			$statement = $this->database->prepare("INSERT INTO
				users (username, email, encrypted_password, completed, salt, registration_date, lastupdated_date) 
				VALUES (:username, :email, :encrypted_password, :completed, :salt, NOW(), NOW())"
				);
		}
		catch (Exception $e) {
			echo "Exception: " . $e;
		}
		
		$passAndSalt = $this->encryptPassword($password, ENCRYPT_COST);
		$enc_password = $passAndSalt['password'];
		$salt = $passAndSalt['salt'];

		try {
			$grej = $statement->execute(
				array(
				':username' => $username, 
				':email' => $email, 
				':encrypted_password' => $enc_password,
				':completed' => "[]",
				':salt' => $salt
				)
			);
		}
		catch (Exception $e) {
			echo "Exception: " . $e;
		}
	}

	// Checks if username exists in database
	// Returns true if it does, false if not.
	public function usernameExists($username) {
		try {
			$statement = $this->database->prepare("SELECT * FROM users WHERE username = ?");
			$statement->execute(array($username));
			$rowCount = $statement->rowCount();
		}
		catch (Exception $e) {
			echo "Exception: " . $e;
		}

		if ($rowCount > 0)
			return true;
		else
			return false;
	}

	// Checks if email exists in database.
	// Returns true if it does, false if not.
	public function emailExists($email) {
		try {
			$statement = $this->database->prepare("SELECT * FROM users WHERE email = ?");
			$statement->execute(array($email));
			$rowCount = $statement->rowCount();
		}
		catch (Exception $e) {
			echo "Exception: " . $e;
		}

		if ($rowCount > 0)
			return true;
		else
			return false;
	}

	// Returns a hash for the password and the salt.
	// Encrypted with Blowfish, 22 characters
	public function hashPassword($password, $salt) {
		return crypt($password, $salt);
	}

	// Encrypts password with random salt.
	// Returns associative array with both salt and encrypted password.
	public function encryptPassword($password, $cost) {
		$blowfishSaltHeader = '$2a$' . $cost . '$';
		$saltCharacters = $this->randomSalt(22);
		$salt = $blowfishSaltHeader . $saltCharacters;

		$encryptedPassword = crypt($password, $salt);

		echo "Encrypted passwword: " . $encryptedPassword . "\n";
		echo "Salt: " . $salt . "\n";

		$passwordAndSalt = array("password" => $encryptedPassword, "salt" => $salt);
		return $passwordAndSalt;
	}

	// Randomly generates a salt in the Blowfish alphabet with a length of $length
	private function randomSalt($length) {
		$saltCharacterPool = array_merge(range('A', "Z"), range('a', 'z'), range(0, 9));
		$salt = "";

		for ($i = 0; $i < $length; $i++) {
			$salt .= $saltCharacterPool[array_rand($saltCharacterPool)];
		}

		return $salt;
	}

	// Update the experience points of $username
	public function updateExperience($userId, $experience) {
		try {
			$statement = $this->database->prepare("UPDATE users SET experience = ?, lastupdated_date = NOW() WHERE id = ?");
			$statement->execute(array($experience, $userId));
		}
		catch (Exception $e) {
			echo "Exception: " . $e;
		}
	}

	// Add mission to completed list of user with $userId.
	public function addCompletedMission($userId, $mission_id) {
		try {
			$statement = $this->database->prepare("SELECT completed FROM users WHERE id = ?");
			$statement->execute(array($userId));
			$result = $statement->fetch(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {
			echo "Exception: " . $e;
		}

		$missionArray = json_decode($result['completed']);

		if (!$this->alreadyCompleted($missionArray, $mission_id)) {
			settype($mission_id, "int");
			array_push($missionArray, $mission_id);
			$this->updateCompletion($userId, $missionArray);

		}
	}

	public function alreadyCompleted($missionArray, $mission_needle) {
		if (empty($missionArray)) {
			return false;
		}
		foreach ($missionArray as $mission_haystack) {
			if ($mission_id == $mission_haystack) {
				return true;
			}
		}
		return false;
	}

	// Update the completed missions list of $username
	public function updateCompletion($userId, $missions) {
		try {
			$statement = $this->database->prepare("UPDATE users SET completed = ?, lastupdated_date = NOW() WHERE id = ?");
			$completion = $this->serializeCompletion($missions);
			$statement->execute(array($completion, $userId));
		}
		catch (Exception $e) {
			echo "Exception: " . $e;
		}
	}

	// Private function to serialize the completion list for storing in the database.
	private function serializeCompletion($missions) {
		foreach ($missions as $value) {
			if (is_numeric($value) == false) {
				throw new Exception("Mission list entry was non-numeric.");
			}
		}

		return json_encode($missions);
	}

	// Private function to deserialize the completion list stored in the database.
	private function deserializeCompletion($serialized) {
		return json_decode($serialized);
	}


	//////////////////////////////////////////////////////////////
	// Mission managing methods, used for managing missions.    //
	//////////////////////////////////////////////////////////////

	public function removeMission($id) {
		if ($this->idExists($id) == false) {
			return false;
		}

		try {
			$statement = $this->database->prepare("DELETE FROM missions WHERE mission_id = ?");
			$statement->execute(array($id));
		}
		catch (Exception $e) {
			echo "Exception: " . $e;
		}

		return true;
	}

	public function addMission($id, $title, $level) {
		if ($this->idExists($id) == true) {
			return false;
		}

		try {
			$statement = $this->database->prepare("INSERT INTO missions 
				(mission_id, title, level)
				VALUES (?, ?, ?)");
			$statement->execute(array($id, $title, $level));
		}
		catch (Exception $e) {
			echo "Exception: " . $e;
		}

		return true;
	}

	public function getMissions($excludeIdList) {
		$whereClause = "";
		if (!empty($excludeIdList)) {
			$implodedExclude = implode(",", $excludeIdList);
			$whereClause = " WHERE mission_id NOT IN (" . $implodedExclude . ")";
		}
		try {
			$statement = $this->database->query("SELECT mission_id, title, type, level FROM missions" . $whereClause);
			$missions = $statement->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {
			echo "Exception: " . $e;
		}

		return $missions;
	}

	public function idExists($id) {
		echo "Id being checked: " . $id . "\n";

		try {
			$statement = $this->database->prepare("SELECT * FROM missions WHERE mission_id = ?");
			$statement->execute(array($id));
			$rowCount = $statement->rowCount();
		}
		catch (Exception $e) {
			echo "Exception: " . $e;
		}

		echo "rowCount: " . $rowCount;

		if ($rowCount <= 0) {
			echo "Id doesn't exists.\n";
			return false;
		} 
		else {
			echo "Id does exist.\n";
			return true;
		}
	}

	//////////////////////////////////////////////////////////////
	// Picture upload methods, used for managing pictures.      //
	//////////////////////////////////////////////////////////////


	public function getRandom() {
		try {
			$statement = $this->database->prepare(
				"SELECT 
				pictures.id, 
				pictures.author_id, 
				pictures.mission_id, 
				users.username, 
				pictures.image_path 
				FROM pictures 
				INNER JOIN users 
				ON (pictures.author_id = users.id)");
			$statement->execute(array());

			$result = $statement->fetchAll(PDO::FETCH_ASSOC);
			$rowCount = $statement->rowCount();
		}
		catch (Exception $e) {
			echo "Exception: " . $e;
		}

		if ($rowCount > 0) {
			$randomId = rand(0, $rowCount - 1);

			return $result[$randomId];
		}
		else
			return null;
	}

	public function alreadySubmitted($author_id, $mission_id) {
		try {
			$statement = $this->database->prepare("SELECT * FROM pictures WHERE author_id = ? AND mission_id = ?");
			$statement->execute(array($author_id, $mission_id));
			$rowCount = $statement->rowCount();
		}
		catch (Exception $e) {
			echo "Exception: " . $e;
		}

		if ($rowCount > 0) {
			return true;
		}
		else {
			return false;
		} 
	}

	public function addPicture($author_id, $image_path, $mission_id) {
		try {
			$statement = $this->database->prepare(
				"INSERT INTO pictures
				(ratings_num, ratings_sum, author_id, image_path, created_date, mission_id)
				VALUES (:ratings_num, :ratings_sum, :author_id, :image_path, NOW(), :mission_id)"
			);

			$statement->execute(
				array(
					':ratings_num' => 0,
					':ratings_sum' => 0,
					':author_id' => $author_id,
					':image_path' => $image_path,
					':mission_id' => $mission_id)
			);
		}
		catch (Exception $e) {
			echo "Exception: " . $e;
		}
	}


	////////////////////////////////////////////////////////////////////
	// Comment methods, for uploading and downloading  comments.      //
	////////////////////////////////////////////////////////////////////

	public function addComment($picture_id, $user_id, $comment) {
		try {
			$statement = $this->database->prepare(
				"INSERT INTO comments 
				(picture_id, user_id, comment, created_date) 
				VALUES (:picture_id, :user_id, :comment, NOW())");
			$statement->execute(
				array(
					':picture_id' => $picture_id,
					':user_id' => $user_id,
					':comment' => $comment)
			);
		}
		catch (Exception $e) {
			echo "Exception: " . $e;
		}
	}

	public function getComments($picture_id) {
		try {
			$statement = $this->database->prepare(
				"SELECT comments.id,
				comments.user_id,
				comments.picture_id,
				comments.comment,
				comments.created_date,
				users.username
				FROM comments 
				INNER JOIN users 
				ON (users.id = comments.user_id)
				WHERE comments.picture_id = ?");
			$statement->execute(array($picture_id));
			$rowCount = $statement->rowCount();

			$result = $statement->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {
			echo "Exception: " . $e;
		}


		return $result;
	}



}

?>