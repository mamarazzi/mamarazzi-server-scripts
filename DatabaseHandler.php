<?php

class DatabaseHandler {
	
	private $database = 0;

	
	public function __construct() {
	}

	public function connect($dns, $username, $password) {
		try {
			$this->database = new PDO($dns, $username, $password);
		}
		catch (Exception $e) {
			echo "Connection failed: " . $e->getMessage();
		}
	}

	public function getPDO() {
		if ($this->database == 0) {
			throw new Exception("No connection has been made.");
		}
		else {
			return $this->database;
		}
	}
}

?>