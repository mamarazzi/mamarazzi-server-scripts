<?php

if (isset($_POST['request']) && $_POST['request'] != "") {
	require_once "MamarazziDatabase.php";
	$database = new MamarazziDatabase();

	

	if ($_POST['request'] == "upload" && isset($_FILES['picture'])) {
		$response = array('request' => $_POST['request']);

		if ($database->alreadySubmitted($_POST['author_id'], $_POST['mission_id']) == false) {
			$pictureDir = "/pictures/";
			$uniqueName = uniqid() . ".png";
			$destinationDir = $pictureDir . $uniqueName;

			$tempDir = $_FILES['picture']['tmp_name'];

			if (move_uploaded_file($tempDir, "." . $destinationDir) === true)  {
				$database->addPicture(
				$_POST['author_id'],
				$destinationDir,
				$_POST['mission_id']);

				$database->addCompletedMission($_POST['author_id'],  $_POST['mission_id']);

				$response['author_id'] = $_POST['author_id'];
				$response['mission_id'] = $_POST['mission_id'];
				$response['success'] = true;
			}
			else {
				$response['success'] = false;
				$response['error'] = 1;
				$response['error_message'] = "File could not be uploaded.";
			}
		}
		else {
			$response['success'] = false;
			$response['error'] = 2;
			$response['error_message'] = "User has already completed this mission.";
		}

		echo json_encode($response);
	}
	else if ($_POST['request'] == "get") {
		$response = array('request' => $_POST['request']);

		$picture = $database->getRandom();

		if ($picture != null) {
			$response['success'] = true;
			$response['pictures'] = array($picture);
		}
		else {
			$response['success'] = false;
		}

		echo json_encode($response);
	}
}
else {

	echo "Error: No request was made";
}

?>